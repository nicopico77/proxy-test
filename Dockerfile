# syntax=docker/dockerfile:1
# specify the node base image with your desired version node:<version>
FROM node:18-alpine

WORKDIR /proxy-test

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install

COPY . .

USER node

# replace this with your application's default port.
EXPOSE 8080

CMD [ "npm", "start" ]