const express = require("express");
const { createProxyMiddleware } = require("http-proxy-middleware");

const app = express();

const PORT = 8080;
const HOST = "localhost";
const API_SERVICE_URL = "https://api.spoonacular.com/";

app.get("/complexSearch", (req, res, next) => {
  console.log("RES", res);

  res.send(res.json())
});

app.use("", (req, res, next) => {
  req.headers["x-api-key"] = "648c0b5205694f9ea8285b70aa327b0a";
  if (req.headers["x-api-key"]) {
    next();
  } else {
    res.sendStatus(403);
  }
});

app.use(
  "/recipes",
  createProxyMiddleware({
    target: API_SERVICE_URL,
    changeOrigin: true,
    pathRewrite: {
      "^/recipes": "/recipes", // rewrite path
    },
    onProxyReq: (proxyReq, req, res) => {
      console.log("REQUEST HEADERD", req.headers);
    },
    onProxyRes:(proxyRes, req, res) => {
        console.log("proxy CODE", proxyRes.statusCode);
        console.log("RES CODE", res.statusCode);
      },
  })
);

app.listen(PORT, HOST, () => {
  console.log(`Proxy started ad ${HOST}:${PORT}`);
});
